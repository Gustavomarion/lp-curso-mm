<?php

date_default_timezone_set('America/Sao_Paulo');
if( empty($remetentes_adicionais)) { $remetentes_adicionais = array(); }
$total_remetentes_adicionais = count($remetentes_adicionais);

/**
  * SaMUS: MDM
  * Componente para enviar mensagens usando a função padrão 'mail' do PHP
  * Versão 1.0.0
  * Criado em: 12-2017 - V1.0.0
  * Última modificação em: 12-2017
  * Modificações Backlog:
		* Nenhum registro
*/

class MMail { 
	
	/**
	  * template: modelo HTML a ser enviado para o cliente
	  * variaveis: array com as key,value que serão processadas no template
	  	* as variaveis também são gravadas nos leads
	  * lead: uasdo internamente (linha do txt de leads)
	  * encode_msg: encoda o texto enviado pelo input do cliente
	  * decode_msg: decoda o texto enviado pelo input do cliente
	  * grava_lead: true ou false (adiciona uma linha ao txt de leads)
	*/
	
	public $template;
	public $variaveis;
	private $lead;
	public $encode_msg;
	public $decode_msg;
	public $grava_lead;
	
	public function montaMensagem() {
		
		/**
		  * Substitui as variaveis no template selecionado
		  * Pode ser que o template possua acentos e precise ser trabalhado
		*/
		
		if(!empty($this->template)){
			$mensagem = file_get_contents($this->template);
			
			/*
		  	  * Procura pelas variáveis disponíveis
		  	  * e substitui no template selecionado
			*/
		
			$this->lead = "";
			foreach($this->variaveis as $chave => $valor):
				
				$valor_lead = $valor;
			
				/**
		  		  * Configura o e-mail usando utf8_encode, decode ou nada.
				*/
			
				if($this->encode_msg){ $valor = utf8_encode($valor); } 
			 	else if ($this->decode_msg){ $valor = utf8_decode($valor); }
			
				$this->lead .= '['.strip_tags($chave).'] ' . strip_tags($valor_lead) . ', ';
				$mensagem = str_replace("{".$chave."}", $valor, $mensagem);
											
			endforeach;
				
		} else {
			
			$mensagem = "";
			
			/*
		  	  * Procura pelas variáveis disponíveis
		  	  * e adiciona ao corpo do e-mail
			*/
		
			foreach($this->variaveis as $chave => $valor):
				
				$valor_lead = $valor;
			
				/**
		  		  * Configura o e-mail usando utf8_encode, decode ou nada.
				*/
			
				if($this->encode_msg){ $valor = utf8_encode($valor); }
				else if ($this->decode_msg){ $valor = utf8_decode($valor); }
			
				$this->lead .= '['.strip_tags($chave).'] ' . strip_tags($valor_lead) . ', ';
				$mensagem .= "<p><strong>{$chave}:</strong> {$valor}</p>";
			
			endforeach;
			
		}
		
		return $mensagem;
		
	}
	
	/**
	  * Gera arquivo de texto com os leads
	  * capturados no envio do formulário
	  * padrão de gravação: true
	*/
	
	public function gravaLead()
	{
		
		/**
		  * Cria um txt com criptografia
		  * dificultando o acesso via navegador
		*/
		
		$root = md5( $_SERVER['HTTP_HOST'] );
		$secret_url = $_SERVER['DOCUMENT_ROOT'];
		$public_url = $secret_url;
		
		/**
		  * Tenta gravar abaixo da raiz, para ficar inacessível via URL
		*/

		$d = date('m-Y');
		$secret_url = str_replace('public_html','', $secret_url);
		$secret_url = str_replace('www','', $secret_url);
		$secret_url = "{$secret_url}/{$root}.{$d}.txt";
		$secret_url = str_replace('//', '/', $secret_url);
	
		$public_url .= "/{$root}.{$d}.txt";
		
		/**
		  * Monta a linha de texto
		*/
		
		$this->lead = substr( $this->lead, 0, -2) . PHP_EOL;
		$this->lead = '['.date('d-m-Y').']['.date('h:i:s').'] ' . $this->lead;
		
		/**
		  * Tenta abrir a URL abaixo da raiz
		  * Caso não consiga, grava o lead na pasta do componente
		*/
		
		$lead = @fopen("{$secret_url}", "a");
		
		if ( !$lead ) {
        	$lead = fopen("{$public_url}", "a");
		}
		
		fwrite($lead, $this->lead);
		fclose($lead);
	}
}

/**
  * MMail
  * Processamento da mensagem
  * Processamento dos leads
  * Processamento da acentuação
*/

$MMail = new MMail;

/**
  * Monta o HTML da mensagem
*/

$MMail->variaveis = $variaveis;
$MMail->template = $template_email;
$MMail->decode_msg = true;
$mensagem = $MMail->montaMensagem();	

/**
  * SimpleMail PHP
  * Estrutura adequada para envio de email
*/


$headers = 
	'From: '.$email_remetente.'' . "\r\n" .
	'Reply-To: '.$email_remetente.'' . "\r\n" .
	'MIME-Version: 1.1' . "\r\n" .
    'Content-type: text/html; charset=UTF-8' . "\r\n" .
	'X-Mailer: PHP/' . phpversion();
	


if (mail( $para, $assunto, $mensagem, $headers, '-f'.$email_remetente.'')){

	
	
	/**
	  * Gravando o lead
	*/
	// print_r($MMail->gravaLead());

	if( $grava_lead ){ $MMail->gravaLead(); }


		if( !empty($alert_sucesso) and !empty($retorno_sucesso) ){


		if( !empty($alert_sucesso) ){ ?>

			<script>
				alert('<?php echo $alert_sucesso; ?>');
				window.location.href = '<?php echo $retorno_sucesso; ?>';
			</script>

		<?php } else { ?>

			<script>
				window.location.href = '<?php echo $retorno_sucesso; ?>';
			</script>

		<?php }

		}
	
} else { ?>
	
	<script>
		alert('<?php echo $alert_erro; ?>');
		history.go(-1);
	</script>
	
<?php } ?>