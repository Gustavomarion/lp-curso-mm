<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

//ini_set('SMTP','localhost');
//ini_set('smtp_port',25);
//ini_set('sendmail_from','aguasdeigarata@aguasdeigarata.com.br');

/*
 * Variaveis do Post
 */

$tema = $_POST['tema'];
$nome = $_POST['nome'];
$empresa = $_POST['empresa'];
$email = strtolower($_POST['email']);
$telefone = $_POST['telefone'];
$mensagem = nl2br($_POST['mensagem']);

/**
 * Configuração inicial
 * Este arquivo contém as variáveis necessárias
 * Para disparo seguro e confiável da mensagem
 * Troque as variáveis pelos dados corretos
 * Cabeçalhos adicionais aceitam email,nome (nessa ordem e formato)
 */

$nome_remetente = utf8_decode("Volt");
$email_remetente = "contato@volt.site";
$assunto = "Contato do Website | {$nome_remetente}";


$para = "contato@volt.site";
$nome_para = utf8_decode("Volt");

/**
 * Mensagens de erro e sucesso (alertas javascript)
 * Para quebra de linhas, inserir \\n
 */

$alert_sucesso = "Mensagem enviada com sucesso";
$alert_erro = "Houve um erro ao processar a mensagem, por favor, tente novamente";
$retorno_sucesso = "http://volt.site";

/**
 * Modelo em HTML que será
 * utilizado para envio da mensagem
 */

$template_email = "componentes/mail/templates/modelo-contato.html";

/**
 * Codificação do arquivo (template)
 * geralmente iso-8859-1 ou utf-8
 */

$charset = "iso-8859-1";

/**
 * Se usar envio autenticado, preencher as credenciais
 * Se vazio, considera envio padrão de e-mail
 */

$credenciais_servidor = array(
    'host' => 'mail.aguasdeigarata.com.br',
    'porta' => 587,
    'usuario' => 'contato-site@aguasdeigarata.com.br',
    'senha' => 'w&U=rFbyW(4{',
);

/**
 * Cabeçalhos adicionais
 * copia: Carbon Copy
 * copia_oculta: Blind Carbon Copy
 * responder_para: endereço de resposta
 * de: endereço do remetente

 * Preenchimento: somente e-mail ou 'email', 'nome'
 * Ou array vazio para false
 */

$replyto_nome = utf8_decode($nome);

$cabecalhos_adicionais = array(
    'reply-to' => array('nome' => $replyto_nome, 'email' => $email),
);

/**
 * Gera linha no arquivo de leads
 * Ao final do envio da mensagem
 * true ou false
 */

$grava_lead = true;

/**
 * Envia anexo através do e-mail
 * nome do arquivo ou vazio
 */

$anexo = "";

$erro = false;
$msgErro = array();

if (empty($tema)) {
    $erro = true;
    $msgErro[] = "O assunto precisa ser preenchido";
}

if (empty($nome)) {
    $erro = true;
    $msgErro[] = "O nome precisa ser preenchido";
}

if (empty($empresa)) {
    $erro = true;
    $msgErro[] = "A empresa precisa ser preenchido";
}

if (empty($email)) {
    $erro = true;
    $msgErro[] = "O e-mail precisa ser preenchido";
}

if (empty($telefone)) {
    $erro = true;
    $msgErro[] = "O Telofone precisa ser preenchido";
}

if (empty($mensagem)) {
    $erro = true;
    $msgErro[] = "O Mensagem precisa ser preenchido";
}

if ($erro) {

    echo '<h1>Houve algum problema ao enviar a mensagem</h1>';
    echo '<ul>';

    foreach ($msgErro as $valor):
        echo "<li>{$valor}</li>";
    endforeach;

    echo '</ul>';
    echo '<hr />';
    echo "<p><a href='javascript:history.go(-1);'>Tente novamente</a></p>";

    exit;
}

$variaveis = array(
    'tema' => $tema,
    'nome' => $nome,
    'empresa' => $empresa,
    'email' => $email,
    'telefone' => $telefone,
    'mensagem' => $mensagem,
    'ip' => $_SERVER['REMOTE_ADDR'],
);

require_once 'componentes/mail/mail.php';
