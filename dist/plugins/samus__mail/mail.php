<?php

date_default_timezone_set('America/Sao_Paulo');
if( empty($remetentes_adicionais)) { $remetentes_adicionais = array(); }
$total_remetentes_adicionais = count($remetentes_adicionais);

/*
  * Componente para enviar mensagens
  * Versão 1.1.6
  * Criado em: nov-2016
  * Atualização: 09-12-2016
  * Atualização: 03-04-2017 (Host Credentials)
  * Atualização: 24-04-2017 (Correção de bugs com acentuação e debugs)
  * Atualização: 08-09-2017 (Integração com PHP Mailer)
  * Atualização: 09-09-2017 (Troca total da classe, para integrar ao PHPMailer)
  * Atualização: 10-09-2017 (email + nome cabeçalhos adicionais)
  * Atualização: 12-12-2017 (emails transacionais)
  * Atualização: 20-12-2017 (bugfix: emails adicionais)
  * Atualização: 02-05-2018 (bugfix: ajustes no reply-to e na facilidade de uso)
  * Última atualização: 02-05-2018
  * Marcelo Motta (www.mdmstudio.com.br)
*/

class MMail { 
	
	/**
	  * template: modelo HTML a ser enviado para o cliente
	  * variaveis: array com as key,value que serão processadas no template
	  	* as variaveis também são gravadas nos leads
	  * lead: uasdo internamente (linha do txt de leads)
	  * encode_msg: encoda o texto enviado pelo input do cliente
	  * decode_msg: decoda o texto enviado pelo input do cliente
	  * grava_lead: true ou false (adiciona uma linha ao txt de leads)
	*/
	
	public $template;
	public $variaveis;
	private $lead;
	public $encode_msg;
	public $decode_msg;
	public $grava_lead;
	
	public function montaMensagem() {
		
		/**
		  * Substitui as variaveis no template selecionado
		  * Pode ser que o template possua acentos e precise ser trabalhado
		*/
		
		if(!empty($this->template)){
			$mensagem = file_get_contents($this->template);
			
			/*
		  	  * Procura pelas variáveis disponíveis
		  	  * e substitui no template selecionado
			*/
		
			$this->lead = "";
			foreach($this->variaveis as $chave => $valor):
				
				$valor_lead = $valor;
			
				/**
		  		  * Configura o e-mail usando utf8_encode, decode ou nada.
				*/
			
				if($this->encode_msg){ $valor = utf8_encode($valor); } 
			 	else if ($this->decode_msg){ $valor = utf8_decode($valor); }
			
				$this->lead .= '['.strip_tags($chave).'] ' . strip_tags($valor_lead) . ', ';
				$mensagem = str_replace("{".$chave."}", $valor, $mensagem);
											
			endforeach;
				
		} else {
			
			$mensagem = "";
			
			/*
		  	  * Procura pelas variáveis disponíveis
		  	  * e adiciona ao corpo do e-mail
			*/
		
			foreach($this->variaveis as $chave => $valor):
				
				$valor_lead = $valor;
			
				/**
		  		  * Configura o e-mail usando utf8_encode, decode ou nada.
				*/
			
				if($this->encode_msg){ $valor = utf8_encode($valor); }
				else if ($this->decode_msg){ $valor = utf8_decode($valor); }
			
				$this->lead .= '['.strip_tags($chave).'] ' . strip_tags($valor_lead) . ', ';
				$mensagem .= "<p><strong>{$chave}:</strong> {$valor}</p>";
			
			endforeach;
			
		}
		
		return $mensagem;
		
	}
	
	/**
	  * Gera arquivo de texto com os leads
	  * capturados no envio do formulário
	  * padrão de gravação: true
	*/
	
	public function gravaLead()
	{
		
		/**
		  * Cria um txt com criptografia
		  * dificultando o acesso via navegador
		*/
		
		$root = md5( $_SERVER['HTTP_HOST'] );
		$secret_url = $_SERVER['DOCUMENT_ROOT'];
		$public_url = $secret_url;
		
		/**
		  * Tenta gravar abaixo da raiz, para ficar inacessível via URL
		*/

		$d = date('m-Y');
		$secret_url = str_replace('public_html','', $secret_url);
		$secret_url = str_replace('www','', $secret_url);
		$secret_url = "{$secret_url}/{$root}.{$d}.txt";
		$secret_url = str_replace('//', '/', $secret_url);
	
		$public_url .= "/{$root}.{$d}.txt";
		
		/**
		  * Monta a linha de texto
		*/
		
		$this->lead = substr( $this->lead, 0, -2) . PHP_EOL;
		$this->lead = '['.date('d-m-Y').']['.date('h:i:s').'] ' . $this->lead;
		
		/**
		  * Tenta abrir a URL abaixo da raiz
		  * Caso não consiga, grava o lead na pasta do componente
		*/		
		
		$lead = @fopen("{$secret_url}", "a");
		
		if ( !$lead ) {
        	$lead = fopen("{$public_url}", "a");
		}
		
		fwrite($lead, $this->lead);
		fclose($lead);
		
	}
}

require_once('php-mailer-novo/src/PHPMailer.php');

/**
  * MMail
  * Processamento da mensagem
  * Processamento dos leads
  * Processamento da acentuação
*/

$MMail = new MMail;

/**
  * Monta o HTML da mensagem
*/

$MMail->variaveis = $variaveis;
$MMail->template = $template_email;
$MMail->decode_msg = true;
$mensagem = $MMail->montaMensagem();	



$mail = new PHPMailer();


/* Reply-to precisa vir ANTES do FROM NAME para funcionar adequadamente */

if( !empty($cabecalhos_adicionais) ){
	
	if( !empty($cabecalhos_adicionais['reply-to']) ){
		$mail->AddReplyTo( $cabecalhos_adicionais['reply-to']['email'], $cabecalhos_adicionais['reply-to']['nome'] );
	}
	
	if( !empty($cabecalhos_adicionais['bcc']) ){
		$mail->AddAddress( $cabecalhos_adicionais['bcc']['email'], $cabecalhos_adicionais['bcc']['nome'] );
	}
	
	if( !empty($cabecalhos_adicionais['cc']) ){
		$mail->AddAddress( $cabecalhos_adicionais['cc']['email'], $cabecalhos_adicionais['cc']['nome'] );
	}
	
}


$mail->AddAddress( $para_01, $nome_para_01);
$mail->AddAddress( $para_02, $nome_para_02);
// $mail->AddAddress( $para_03, $nome_para_03);


if( !empty($credenciais_servidor) ){


	$mail->IsSMTP();
	$mail->Host 	= $credenciais_servidor['host'];
	$mail->Port 	= $credenciais_servidor['porta'];
	$mail->SMTPAuth = true;
	$mail->Username = $credenciais_servidor['usuario'];
	$mail->Password = $credenciais_servidor['senha'];
	$mail->SetFrom( $credenciais_servidor['usuario'], $nome_remetente);
	
} else {

	
	$mail->SetFrom( $email_remetente, $nome_remetente );
	
}


$mail->WordWrap = 50;
$mail->IsHTML(true);
$mail->Subject = $assunto;
$mail->Body = "$mensagem";
// $mail->SMTPDebug = 2;

//Verifica se o e-mail foi enviado

if(!$mail->Send())
{


echo "Mensagem não enviada";
echo "Mailer Error: " . $mail->ErrorInfo;



} else {

	$MMail->gravaLead();
	
	$resultado = array(
		'status' => 200,
		'mensagem' => 'Mensagem enviada com sucesso',
		'debug' => 'OK'
	);
	
	return json_encode( $resultado );
	exit;

}